# OL - projet pour manipuler git

## Sommaire

* [[presentation.md|présentation générale de l'Olyympique Lyonnais]]
* [[historique.md|historique de l'Olympique Lyonnais]]
* [[epopee-formidable-aventure-humaine.md|la grande épopée des années 2000]]
* [[EBITDA.md| définition de l'EBITDA et de l'amortissement sur ressources]]
